// mongoose.connect('mongodb://localhost/renuar', { is found in app.js
// which in fact connects renuar with clothes (Case sennsitive not...)
// make sure its in ts to be createdd in dist
var mongoose = require('mongoose');

var ItemSchema = new mongoose.Schema({
  mRetailerName: String,
  mId: String,
  mPage: String,  // path to web page of item
  mTitle: String,
  mDescription: String,
  mImages: [String],
  mCondition: String,
  mStock: String,
  mAge: String,
  mGender: String,
  mPrice: String,
  mSalePrice: String,
  mCategory: {
    name: String,
    vals: [{  _id: false,
              name: String,
              vals : [String],
              maxSelectCount: [Number] 
            }]
  }
  });
module.exports = mongoose.model('Items', ItemSchema);