# MEAN Stack Angular 9 Build Realtime CRUD Web App Quickly

This source code is part of [MEAN Stack Angular 9 Build Realtime CRUD Web App Quickly](https://www.djamware.com/post/5e50b735525fc968b04a707f/mean-stack-angular-9-build-realtime-crud-web-app-quickly).



3. in AskLily\asklily_server_new
3.0 npm install (once)
3.1 tsc -w

Load items to db

mongoimport --host localhost:27017  --db askLily --collection items --type json --file latest.json --jsonArray

// local 
mongoexport --host localhost:27017 --collection=items --db=askLily --out=latest.json 
-- need to enclose the data in [ and add , after each record ( replace '} {' with '}, { ') ]
mongoimport --host localhost:27017  --db askLily --collection items --type json --file C:\AskLily\_data\latest.json --jsonArray

// server
mongoexport --collection=items --db=askLily --out=latest.json 
mongoimport --db NewAskLily --collection items --type json --file fixeddb.json --jsonArray


How to move to new items:
How to update data base
ON THE SERVER MAKE SURE: rename collection
mongoimport --host localhost:27017  --db askLily --collection items --type json --file C:\AskLily\_data\single.json --jsonArray
Run application 
load latest.json
run update db.
