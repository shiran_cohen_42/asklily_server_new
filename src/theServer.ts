import express = require('express');
import { ItemsService } from './itemsService';
import { CUser } from '../../gam_ve_gam/CUser';

const app: express.Application = express();

app.listen(3000, function () {
    console.log('Server is listening on port 3000!');
    });
    
var items = require('../models/itemsModel');

var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');



app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

let localItems: ItemsService;

let count = 0;

mongoose.connect('mongodb://localhost/askLily', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
}).then(() =>  {
    console.log('Mongoose connection successful');
     localItems = new ItemsService(items);
    })
  .catch((err) => console.error(err));

// list data
app.get('/api/', function(req, res, next) {
    // console.log('Before calllingg db');
    try {
        items.find(function (err, dbItems) {
            // console.log('after call to find');
            if (err) return next(err);
            res.json(dbItems);
        }); } 
     catch ( e ) {
         console.log('Failed');
     }
});

app.get('/items/:retailer_gennder', function(req, res, next) {
    console.log('items :', req.params.retailer_gennder);
    const parts = req.params.retailer_gennder.split('_');
    console.assert( parts.length === 2);

    localItems.getRetailerGender(parts[0], parts[1]);
    localItems.doneLoading()
    .then( () => {
            res.json(localItems.mRetailerList);
        }
    )
    .catch( error => console.log(error))
});


app.post('/ask/', function(req, res, next) {
    console.log('ask : ',  count++);
    console.assert(req.body[1].name === 'User');
    const retail = req.body[0];
    const user = new CUser(req.body[1].vals);
    const ugen = user.getGender();
    const debug = req.body[2];

    localItems.getRetailerGender(retail, ugen);
    localItems.doneLoading()
    .then( () => {
            const results = localItems.AskLily(user, retail, ugen, debug);
            res.json(results);
        }
    )
    .catch( error => console.log(error))
    
});

// get data by id
app.get('/api/:id', function(req, res, next) {
    items.findById(req.params.id, function (err, dbItems) {
        if (err) return next(err);
        res.json(dbItems);
    });
});
  
// post data
app.post('/api/', function(req, res, next) {
    items.create(req.body, function (err, dbItems) {
        if (err) {
            console.log(err);
            return next(err);
        }
        res.json(dbItems);
    });
    localItems.setNeedToReload();
});

// post data
app.post('/api/_', function(req, res, next) {
    items.insertMany(req.body, function (err, dbItems) {
        if (err) {
            console.log(err);
            return next(err);
        }
        res.json(dbItems);
    });
    localItems.setNeedToReload();
});


// put data
app.put('/api/:id', function(req, res) {
    items.findByIdAndUpdate(req.params.id, req.body, 
        function (err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            res.json(result);
        }
    });
    localItems.setNeedToReload();
});
  
// delete data by id
app.delete('/api/:id', function(req, res, next) {
    items.findByIdAndRemove(req.params.id, req.body, function (err, dbItems) {
        if (err) return next(err);
        res.json(dbItems);
    });
    localItems.setNeedToReload();
});


// module.exports = router;