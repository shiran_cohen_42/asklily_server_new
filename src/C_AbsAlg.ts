import { CTruthMatrix } from './C_TruthMatrix';
import { CDictionary } from '../../gam_ve_gam/C_Dictionary';

export class CMatrixFile {
    data: string[][] = [];
    constructor() {
    }
}

export class CAbsAlgData {
    algName: string;
    validators: any;

    getValidators(category: string) {
        const a: any[] = [];  // the validators are different
        return a;
    }
    constructor(name: string) {
        this.algName = name;
        this.validators = null;
    }
}

export class CM0Alg implements CAbsAlgData {
    algName = '';
    validators: any;
    getValidators(category: string): any[] {
        throw new Error('Method not implemented.');
    }

}

export class CTruthMatrixAlg implements CAbsAlgData {
    algName: string;
    validators: CTruthMatrix[] = [];
    constructor(name: string) {
        this.algName = name;
    }

    loadFromJson( data: any) {
        console.assert( this.algName === data.algName);
        data.validators.forEach( (mat: any) => {
            const loMat =  new CTruthMatrix();
            loMat.loadFromJson(mat);
            console.assert(loMat !== null, 'Init alg');
            this.validators.push(loMat);
        });
    }

    

    getValidators(category: string) {
        const retList: CTruthMatrix[] = [];
        this.validators.forEach( v => {
            const valid = v.belongsToCategory(category);
            if ( valid ) {
                retList.push(v);
            }
        });
        //  when no alg exist... assert(retList.length > 0, 'Some validators must belong to :' + category);
        return retList;
    }
}

class C_algInfo {
    start: number;
    end: number;
    cols: number;
    constructor(s: number, e:number, c: number) {
        this.start = s;
        this.end = e;
        this.cols = c;
    }
}

export class COccasionAlgData {
    occationTag = '';
    vals: CTruthMatrix[] = [];
    constructor() {}
    loadFromJson( data: any) {
        this.occationTag = data.occationTag;
        this.vals = [];
        data.vals.forEach( (mat: any) => {
            const lotm = new CTruthMatrix();
            lotm.loadFromJson(mat);
            console.assert(lotm !== null, 'Init alg');
            this.vals.push(lotm);
        });
    }
    
    FindMatrix(input: string[][] ) {
        const boxData: CMatrixFile[] = [];
        const alginfo: C_algInfo[] = [];
        const matSeperator = 'and';
        let start: number;
        let end: number;
        let cols: number;

        start = 1;
        for (let i = 1; i < input.length; i++) {
            if (input[i][0] === matSeperator) {
                cols = 0;
                // tslint:disable-next-line: prefer-for-of
                for (let j = 0; j < input[i].length; j++) {
                    if (input[i][j] === matSeperator) {
                        cols++;
                    }
                }
                end = i;
                alginfo.push( new C_algInfo(start, end, cols));
                start = i + 1;
            }
        }

        // tslint:disable-next-line: prefer-for-of
        for (let n = 0; n < alginfo.length; n++) {
            const mat = new CMatrixFile();
            const startRow = alginfo[n].start;
            const rows = alginfo[n].end - startRow;
            for (let r = 0; r < rows; r++) {
                const line: string[] = [];
                for (let j = 0; j < alginfo[n].cols; j++) {
                    const val = input[startRow + r][j];
                    line.push(val);
                }
                mat.data.push(line);
            }
            boxData.push(mat);
        }

        return boxData;
    }

}

export class COccasionAlg implements CAbsAlgData {
    algName = '';
    validators: COccasionAlgData[] = [];
    constructor() {
    }
    loadFromJson( data: any ) {
        this.algName = data.algName;
        data.validators.forEach( (occ: any) => {
            const loOcc = new COccasionAlgData();
            loOcc.loadFromJson(occ);
            console.assert(loOcc !== null, 'Failed to create OccassionOper');
            this.validators.push(loOcc);
        });
    }



    /*
    two stepss - 1 get validators by user occasion
    the second is to match the item category
    */
    getValidators( occasion: string ) {
        const validators: COccasionAlgData[] = [];
        // take validators for the right item
        this.validators.forEach( val => {
            if ( val.occationTag === occasion ) {
                // found the right occation tag
                validators.push(val);
                // this needs more
            }
        });
        return validators;
    }
}
