import { eAlgs } from '../../gam_ve_gam/C_AbsCategory';

import { CTruthMatrixAlg, COccasionAlg, CAbsAlgData } from './C_AbsAlg';
import { CDictionary } from '../../gam_ve_gam/C_Dictionary';

// tslint:disable-next-line: import-spacing
import * as data from './base_algs.json';


export class CAlgHolder {
    algName: string;
    alg: CAbsAlgData;
    constructor(name: string, alg: CAbsAlgData) {
        this.algName = name;
        this.alg = alg;
    }
}

class CLoc {
    start: number;
    end: number;
    constructor(s: number, e: number) {
        this.start = s;
        this.end = e;
    }
}

export class CAlgsData {
    algs: CAlgHolder[] = [];
    dic!: CDictionary;

    constructor() {
        this.loadFromJson();
    }

    loadFromJson() {
        this.loadMatrixAlg(eAlgs.algBodyShapeBase, 'BodyShapeBase');
        this.loadMatrixAlg(eAlgs.algBodyShapeThin, 'BodyShapeThin');
        this.loadMatrixAlg(eAlgs.algBodyShapeAverage, 'BodyShapeAverage');
        this.loadMatrixAlg(eAlgs.algBodyShapePlus, 'BodyShapePlus');
        this.loadMatrixAlg(eAlgs.algHide, 'hide');
        this.loadOccasion(eAlgs.algOccasion, 'occasion');
    }

    loadMatrixAlg(index: number, name: string) {

        const alg = new CTruthMatrixAlg(name);
        console.assert(name === data[index].algName, 'Expecting the same name');
        alg.loadFromJson(data[index].alg);
        const algholder = new CAlgHolder(name, alg);
        console.assert(alg !== null, 'Init alg ' + name);
        this.algs.push(algholder);
    }

    loadOccasion(index: number, name: string) {
        const alg = new COccasionAlg();
        alg.loadFromJson(data[index].alg);
        const algholder = new CAlgHolder(name, alg);
        console.assert(alg !== null, 'Init alg ' + name);
        this.algs.push(algholder);
    }
}
