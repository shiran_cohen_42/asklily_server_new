import { CdbItem } from './C_DbItem';
import { CUser } from '../../gam_ve_gam/CUser';
import { AskApi } from './CMatch';
import { CAskLilyResults, CItemResult } from '../../gam_ve_gam/C_ResultDef';
import { CDictionary } from '../../gam_ve_gam/C_Dictionary';
import * as serverParams from './server_options.json';
var items = require('../models/itemsModel');

/*
  From now - the server does not load any items until asked
  Get all items should not be used.
  Both the application and the algorithm will use items by retailer, gender and stock
  The application needs all the items, where as the alogorithm needs only items that where tagged
*/
export class ItemsService {
  // mItemList: CdbItem[];
  mRetailerList: CdbItem[];
  mLastQuery = '';
  private dbData = [];
  mDictionary: CDictionary;
  db: any;
  inLoading: boolean;
  forceLoading: boolean;
  algApi: AskApi;

  constructor(itemsSchema) {
    this.algApi = new AskApi()
    this.mRetailerList = [];
    this.mDictionary = new CDictionary();
    this.mDictionary.loadFromJson();

    this.db = itemsSchema;

    this.inLoading = false;
    this.forceLoading = false;
  }

  setNeedToReload() {
    this.forceLoading = true;
    process.stdout.write('u')
  }


  async doneLoading(): Promise<void> {
    while (this.inLoading) {
      await new Promise(res => setTimeout(res, 100));
      console.log('.');
    }
    console.log('Load compleated');
  }


  AskLily(user: CUser, retailer: string, gender: string, debug: boolean): CAskLilyResults {
    console.assert(!this.inLoading);
    const res = new CAskLilyResults(true);

    if (this.mRetailerList.length === 0) {
      console.log('No items for: ', retailer);
      return res;
    }

    console.assert(this.mRetailerList.length > 0, 'Items must have been loaded bedore');
    console.assert(this.mRetailerList[0].mRetailerName === retailer);
    console.assert(this.mRetailerList[0].mGender === gender);

    // loop on on relevant items
    this.mRetailerList.forEach(it => {
      if (it.mCategory.readyForWork()) {
        console.assert(it.inStock());
        const result = this.algApi.matchItem(it.mCategory, user);

        const appRes = new CItemResult();
        appRes.fromMatchResut(result.bMatch, it.mCategory.name, it.mImages[0], it.mPage, it.mId,
          it.mTitle, it.mPrice, it.mSalePrice, result.tellMeWhy());

        if (debug) {
          const categoryIndex = this.mDictionary.getCategoryIndex(it.mCategory.name);

          if (result.bMatch) {
            res.matchList[categoryIndex].push(appRes);
          } else {
            res.rejectList[categoryIndex].push(appRes);
          }
        } else {
          if (appRes.bMatch) {
            res.matchList[0].push(appRes);
          }
        }
      } // ready for workd
    });

    if (debug) {
      return res;
    } else {
      const t = this.orgenizeResultsAsPages(res);
      return t;
    }
  }

  orgenizeResultsAsPages(inRes: CAskLilyResults): CAskLilyResults {
    const rv = new CAskLilyResults(false);

    const itemsPerPage = serverParams.numItemsPerPage;

    let done = false;
    let i = 0;
    let itemsInPage = 0;
    let pageIndex = -1;
    do {
      if (itemsInPage === 0) {
        rv.matchList.push([]);
        pageIndex++;
      }
      const res = inRes.matchList[0][i];
      console.assert(res.bMatch);
      res.story = '';
      rv.matchList[pageIndex].push(res);
      itemsInPage++;
      i++;

      if (i === inRes.matchList[0].length) {
        done = true;
      }

      if (itemsInPage === itemsPerPage) {
        itemsInPage = 0;
      }
    } while (!done);


    return rv;
  }

 
  async getRetailerGender(retailer: string, gender: string) {
    const query = `{mRetailerName: '${retailer}', mGender: '${gender}', mStock: '1'}`;

    if (query === this.mLastQuery && !this.forceLoading) {
      this.inLoading = false;
      return;
    }

    this.inLoading = true;
    this.mRetailerList = [];
    console.log('New querty ' + query);

    await this.db.find((err: any, dbItems: never[]) => {
      if (err) {
        console.error('Failed to get items ' + query);
        return err;
      }
      // console.log( 'Got from db: ' + dbItems.length);
      dbItems.forEach((element: any) => {
        const item = JSON.stringify(element);
        const itDb = new CdbItem(item);
        if (itDb.basic(gender, retailer)) {
          this.mRetailerList.push(itDb);
        }
      });
      console.log('', this.mRetailerList.length);
      this.inLoading = false;
      this.mLastQuery = query;

    }).where('mRetailerName').equals(retailer)
      .where('mGender').equals(gender)
      .where('mStock').equals('1');

  }
}
