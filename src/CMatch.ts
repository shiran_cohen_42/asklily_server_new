import { CAbsCategory, eAlgs } from '../../gam_ve_gam/C_AbsCategory';
import { CUser } from '../../gam_ve_gam/CUser';
import { CM2Result, CMatchResult, CM2p5Result, CSimpleResult } from './C_AlgResults'
import { CAbsAlgData, COccasionAlgData } from './C_AbsAlg';
import { CAlgHolder, CAlgsData} from './C_AlgsData';



export class AskApi {
    matcher: CMatcher;
    constructor() {
        this.matcher = new CMatcher();
    }

    public matchItem( item: CAbsCategory, user: CUser)  {
        const mResult = this.matcher.match(item, user);
        return mResult;
    }
}

/*
{
        userFeatureName: 'Tops',
        xFeature: 'BodyShape', ==> in run time gets the full feature
        yFeature: 'TopShape',  ==> in run time gets the full feature
        logicalOper: 'empty',  for later use
        matrix: [
                { tag: 'T shape', vals: [1, 1, 0, 1, 1, ]},
                { tag: 'Button shirt shape', vals: [0, 0, 1, 1, 1, ]},
                { tag: 'Blouse shape', vals: [1, 1, 0, 0, 1, ]},
                { tag: 'Sweater shape', vals: [1, 1, 1, 1, 1, ]},
        ]},

        // checking for Item.name must be 'Tops'
*/
class CMatcher {
    bodyShape: CBodyShapeMathcer[] = [];
    occasion: COccasionMathcer;
    hide: CBodyShapeMathcer;
    style: CSimpleCategoryMatch;
    ageGroup: CSimpleCategoryMatch;

    constructor() {

        const algsData = new CAlgsData();
       
        for ( let i = eAlgs.algBodyShapeThin; i <= eAlgs.algBodyShapePlus; i++) {
            const tmp = new CBodyShapeMathcer(algsData.algs[i].alg);
            this.bodyShape.push( tmp);

        }

        let tmp = new CBodyShapeMathcer( algsData.algs[eAlgs.algBodyShapeBase].alg );
        this.bodyShape.push(tmp );

        this.occasion = new COccasionMathcer(algsData.algs[eAlgs.algOccasion].alg);
        this.hide = new CBodyShapeMathcer(algsData.algs[eAlgs.algHide].alg);
        this.style = new CSimpleCategoryMatch('Style');
        this.ageGroup = new CSimpleCategoryMatch('AgeGroup');
    }

    match(item: CAbsCategory, user: CUser) {
        const rv = new CMatchResult();

        rv.bMatch = true;
        let res  = this.bodyShape[3].match( item, user);
        rv.algsResults.push(res);
        rv.bMatch = rv.bMatch && res.bMatch;

        const s = user.getBodySize();

        res  = this.bodyShape[s].match( item, user);
        rv.algsResults.push(res);
        rv.bMatch = rv.bMatch && res.bMatch;

        const occ = this.occasion.match( item, user);
        rv.algsResults.push(occ);
        rv.bMatch = rv.bMatch && occ.bMatch;

        res  = this.hide.match(item, user);
        rv.algsResults.push(res);
        rv.bMatch = rv.bMatch && res.bMatch;

        let simple = this.style.match(item, user);
        rv.algsResults.push(simple);
        rv.bMatch = rv.bMatch && simple.bMatch;

        simple = this.ageGroup.match(item, user);
        rv.algsResults.push(simple);
        rv.bMatch = rv.bMatch && simple.bMatch;
        
        return rv;
    }
    
}

// to be used with Style and AgeGroup which share same features
// the Item and user must share at leat one tag in common
export class CSimpleCategoryMatch {
    feature: string;
    mAlg: string;
    constructor(feature: string ) {
        this.feature = feature;
        this.mAlg = feature + ' Match';
    }
    match(item: CAbsCategory, user: CAbsCategory): CSimpleResult {
        const retRes = new CSimpleResult(this.mAlg);

        // defualt return false;
        retRes.bMatch =  false;
        // find the category in both
        const itemFindex = item.getFeatureIndex(this.feature);
        const userFindex = user.getFeatureIndex(this.feature);

        retRes.story = 'Look for ' + user.vals[userFindex].name;

        item.vals[itemFindex].vals.forEach( iv => {
            user.vals[userFindex].vals.forEach( uv => {
                if ( iv === uv ) {
                    retRes.bMatch = true;
                    retRes.story += ' found: ' + iv + ', ';
                }
            });
        });

        if ( !retRes.bMatch ) {
            retRes.story += 'found non in item, ';
        }

        return retRes;
    }

}

export class CBodyShapeMathcer  {
    mAlg: CAbsAlgData;

    constructor(alg: CAbsAlgData  ) {
        this.mAlg = alg;  
    }


    match(item: CAbsCategory, user: CAbsCategory): CM2Result {
        // filter only those matrixes which corespond to item category
        const validators = this.mAlg.getValidators(item.name);
        const retRes = new CM2Result(this.mAlg.algName);

        retRes.item = item.name;
        retRes.user = user.name;
        validators.forEach( v => {
            try {
                const matrixResultInfo = v.getTruthVal(user, item);
                retRes.info.push(matrixResultInfo);
            } catch (e) {
                console.assert(false, 'Occcasion match');
            }
        });

        // convert to boolean
        retRes.bMatch = true;
        retRes.info.forEach( val => {
            retRes.bMatch = retRes.bMatch && val.bMatch;
        });

        return retRes;
    }

    
}

export class COccasionMathcer  {
    mAlg: CAbsAlgData;

    constructor(alg: CAbsAlgData ) {
        this.mAlg = alg;    
    }

    match(item: CAbsCategory, user: CAbsCategory): CM2p5Result {

        const occationTag = this.getOccationTag(user);
        const itemCategory = item.name;
        const validators = this.getValidatorsOCC(occationTag, itemCategory);
        const retRes = new CM2p5Result(this.mAlg.algName);

        retRes.bMatch = true;
        retRes.user = user.name;
        retRes.item = item.name;
        retRes.userTag = occationTag;
        validators.forEach( v => {
            const tmp = new CM2Result(this.mAlg.algName);
            tmp.bMatch = true;
            v.vals.forEach( mat => {
                try {
                    const matrixResultInfo = mat.getTruthVal(user, item);
                    tmp.info.push(matrixResultInfo);
                    tmp.bMatch = tmp.bMatch && matrixResultInfo.bMatch;
                } catch ( e ) {
                    console.assert(false, 'Occasion match');
                }
            });
            retRes.info.push(tmp);
            retRes.bMatch = retRes.bMatch && tmp.bMatch;
        });

        return retRes;
    }

    getValidators( occasion: string ): COccasionAlgData[] {
        const validators: COccasionAlgData[] = [];
        this.mAlg.validators.forEach( occOper => {
            if (occOper.occationTag === occasion ) {
                validators.push(occOper);
            }
        });

        return validators;
    }

    getValidatorsOCC( occasion: string, itemCategory: string ): COccasionAlgData[] {
        const validators: COccasionAlgData[] = [];
        this.mAlg.validators.forEach( occOper => {
            if (occOper.occationTag === occasion ) {
                if (occOper.vals[0].categoryName === itemCategory) {
                    validators.push(occOper);
                }
            }
        });

        return validators;
    }

    getOccationTag(user: CAbsCategory) {
        let tag = '';
        user.vals.forEach( feature => {
            if ( feature.name === 'Occasion') {
                tag = feature.vals[0];
            }
        });
        return tag;
    }

}

