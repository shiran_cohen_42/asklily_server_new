import { CTruthMatrixInfo } from './C_TruthMatrix';


export class CAbsResult {
    bMatch: boolean;
    algName: string;

    constructor(algName: string) {
        this.bMatch = true;
        this.algName = algName;
    }
    toString(bPass: boolean) {}
}

export class CMatchResult {
    bMatch: boolean;
    algsResults: CAbsResult[];
 
    constructor() {
        this.bMatch = true;
        this.algsResults = [];
    }
    toString() {}
    tellMeWhy(): string {
        let rs = '';
        this.algsResults.forEach( res => {
            if ( this.bMatch === res.bMatch ) {
                rs += res.toString(this.bMatch);
            }
        });
        return rs;
    }

}

export class CSimpleResult implements CAbsResult {
    bMatch: boolean;
    algName: string;
    story: string;
    constructor(algName: string) {
        this.story = '';
        this.bMatch = true;
        this.algName = algName;
    }
    header() {
        const str = this.algName.toUpperCase() + ' => ' + this.bMatch + ': ';
        return str;
    }

    toString( bPass: boolean) {
        let str = this.header();
        str += this.story;
        return str;
    }
}

export class CM2Result implements CAbsResult {
    bMatch: boolean;
    algName: string;
    user: string;
    item: string;
    info: CTruthMatrixInfo[];
    constructor(algName: string) {
        this.info = [];
        this.bMatch = true;
        this.algName = algName;
        this.user = this.item = '';
    }
    header() {
        const str = this.algName.toUpperCase() + ' => ' + this.bMatch + ': ';
        return str;
    }

    toStringNoHeader( bPass: boolean): string {
        let str = '';
        this.info.forEach( inf => {
            if ( bPass === inf.bMatch) {
                str += inf.toString(bPass) ;
            }
        });
        str += '\n';
        return str;
    }
    toString( bPass: boolean): string {
        let str = this.header();
        this.info.forEach( inf => {
            if ( bPass === inf.bMatch) {
                str += inf.toString(bPass) ;
            }
        });
        str += '\n';
        return str;
    }
}

export class CM2p5Result implements CAbsResult {
    bMatch: boolean;
    algName: string;
    user = '';
    item = '';
    userTag = '';
    info: CM2Result[];

    constructor(algName: string) {
        this.info = [];
        this.bMatch = true;
        this.algName = algName;
    }
    header() {
        const str = this.algName.toUpperCase() + ' : ' + this.userTag + ' => ' + this.bMatch + ': ';
        return str;
    }

    toString( bPass: boolean) {
        let str = this.header();
        this.info.forEach( m2res => {
            if ( bPass === m2res.bMatch) {
                str += m2res.toStringNoHeader(bPass) ;
            }
        });
        str += '\n';
        return str;
    }
}
