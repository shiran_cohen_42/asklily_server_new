import { CAbsCategory } from '../../gam_ve_gam/C_AbsCategory';

export class CdbItem {
  // tslint:disable-next-line: variable-name
  _id = '';
  mRetailerName = '';
  mId = '';
  mPage = '';  // path to web page of item
  mTitle = '';
  mDescription = '';
  mCategory!: CAbsCategory;
  mImages: string[] = [];
  mCondition = '';
  mStock = '';
  mAge = '';
  mGender = '';
  mPrice = '';
  mSalePrice = '';

  //
  // This is for converting the Mongoose json to CDbItem 
  //
  // There is another one 
  constructor(objInJson: any) {
    const obj = JSON.parse(objInJson);
    this._id = obj._id;
    this.mId = obj.mId;
    this.mRetailerName = obj.mRetailerName;
    this.mTitle = obj.mTitle;
    this.mDescription = obj.mDescription;
    this.mImages = obj.mImages;
    this.mCondition = obj.mCondition;
    this.mStock = obj.mStock;
    this.mAge = obj.mAge;
    this.mGender = obj.mGender;
    this.mPage = obj.mPage;
    this.mPrice = obj.mPrice;
    this.mSalePrice = obj.mSalePrice;
    const cat = obj.mCategory;
    this.mCategory = new CAbsCategory(cat.name, 'i', cat.vals);
    return this;
  }

  categoryName() {
    return this.mCategory.name;
  }
  // need to consolidate Micah!!
  updateCategory(newCat: CAbsCategory) {
    //  this.mCategory = newCat;
    this.mCategory = new CAbsCategory(newCat.name, newCat.type, newCat.vals);
  }
  belongsTo(retailer: string) {
    return this.mRetailerName === retailer;
  }
  basic(gender: string, retailer: string) {
    if (this.mRetailerName !== retailer) {
      return false;
    }
    if (this.mGender !== gender) {
      return false;
    }
    return true;
  }

  inStock(): boolean {
    if (this.mStock === undefined || this.mStock === null) {
      return false;
    }
    if (this.mStock === '1') {
      return true;
    }
    return false;
  }
}
